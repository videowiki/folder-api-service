const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const MODEL_NAME = "folder";

const FolderSchema = new Schema({
  name: { type: String },
  organization: { type: Schema.Types.ObjectId, index: true },
  topFolder: { type: Schema.Types.ObjectId, ref: MODEL_NAME },
  parent: { type: Schema.Types.ObjectId, ref: MODEL_NAME },
  level: { type: Number },
  created_at: { type: Date, default: Date.now, index: true },
  updated_at: { type: Date, default: Date.now },
});

FolderSchema.pre("save", function (next) {
  const now = new Date();
  this.updated_at = now;
  if (!this.created_at) {
    this.created_at = now;
  }
  return next();
});

FolderSchema.statics.isObjectId = (id) => mongoose.Types.ObjectId.isValid(id);

FolderSchema.statics.getObjectId = (id) => mongoose.Types.ObjectId(id);

const Folder = mongoose.model(MODEL_NAME, FolderSchema);

module.exports = { Folder };
