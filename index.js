const mongoose = require("mongoose");

const vwGenerators = require("@videowiki/generators");
const { server, app, createRouter } = vwGenerators.serverGenerator({
  uploadLimit: "500mb",
});
const DB_CONNECTION = process.env.FOLDER_SERVICE_DATABASE_URL;
let mongoConnection;
mongoose.connect(DB_CONNECTION).then((con) => {
  mongoConnection = con.connection;
  con.connection.on("disconnected", () => {
    console.log("Database disconnected! shutting down service");
    process.exit(1);
  });

  const controller = require("./controller")();
  const middlewares = require("./middlewares");

  vwGenerators.healthcheckRouteGenerator({ router: app, mongoConnection })

  const Folder = require("./models").Folder;

  app.use(
    "/db",
    vwGenerators.mongodbCRUDGenerator({ Model: Folder, Router: createRouter() })
  );

  app.all("*", (req, res, next) => {
    if (req.headers["vw-user-data"]) {
      try {
        const user = JSON.parse(req.headers["vw-user-data"]);
        req.user = user;
      } catch (e) {
        console.log(e);
      }
    }
    next();
  });

  app.post(
    "/",
    middlewares.authorizeCreateFolder,
    middlewares.validateCreateFolder,
    controller.createFolder
  );

  app.get("/mainFolders", controller.getOrganizationMainFolders);
  app.get("/:id/breadcrumb", controller.getBreadcrumbFolder);
  app.get("/:id/subfolders", controller.getSubfolders);
  app.get("/:id/moveVideo", controller.getMoveVideoFolder);
  app.put(
    "/:id/name",
    middlewares.validateUpdateName,
    middlewares.authorizeUpdateName,
    controller.updateName
  );
});

const PORT = process.env.PORT || 4000;
server.listen(PORT);
console.log(`Magic happens on port ${PORT}`); // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`);
exports = module.exports = app; // expose app
